from GeoJsonWriter import *
from WordEvaluation import *
from PhraseEvaluation import *
from DetectionEvaluation import *
from Geometry.Polygon import Polygon


__author__ = 'Tan'


# Evaluate the characters, words, phrases in the result.
def text_eval(result_rect_list, gt_list):
    area_threshold = 2.25
    overlap_json = init_output_json_structure()     # The json file records the overlap areas.
    non_overlap_json = init_output_json_structure() # The json file records the rectangles which we don't consider them as correct ones.

    count = 1
    total = len(result_rect_list)

    for rect in result_rect_list:
        count += 1
        result_area = rect.get_area()

        find_overlap = False
        for i in range(0, len(gt_list), 1):
            if find_overlap:
                break

            gt_rect_list = gt_list[i]['rects']
            for j in range(0, len(gt_rect_list), 1):
                gt_rect = gt_rect_list[j]
                gt_area = gt_rect.get_area()

                overlap_polygon = rect.get_overlap_polygon(gt_rect)
                if overlap_polygon is None:
                    pass
                else:
                    overlap_area = overlap_polygon.get_area()
                    overlap_valid = False

                    if overlap_area > 0.5 * gt_area: #result_area <= area_threshold * gt_area and :
                        overlap_valid = True
                        rect.positive = True
                        gt_list[i]['found'][j] = True
                        lev_dist = levenshtein_distance(rect.text, gt_rect.text)
                        gt_list[i]['recognition'][j] = lev_dist[0]
                        gt_list[i]['editing'][j] = [lev_dist[1], lev_dist[2], lev_dist[3]]
                        rect.editing = [lev_dist[1], lev_dist[2], lev_dist[3]]
                        find_overlap = True

                    new_feature = generate_overlap_feature(overlap_polygon, overlap_valid, tag=i)
                    overlap_json['features'].append(new_feature)

    # Record the incorrect rectangles.
    for i in range(0, len(result_rect_list)):
        rect = result_rect_list[i]
        if rect.positive == False:
            non_overlap_json['features'].append(generate_overlap_feature(Polygon(rect.points), False, i))

    print 'Detection Precision:       %f' % (get_detection_precision(result_rect_list))
    print 'Detection Recall:          %f' % (get_detection_recall(gt_list))
    print 'Character Edit Distance:   %d' % (get_characters_edit_distance(gt_list))
    print 'Phrase Detection Recall:   %f' % (get_phrase_detection_recall(gt_list))
    print 'Phrase Recognition Recall: %f' % (get_phrase_recognition_recall(gt_list))

    get_word_precision(result_rect_list)
    get_word_recall(gt_list)
    return overlap_json, non_overlap_json
