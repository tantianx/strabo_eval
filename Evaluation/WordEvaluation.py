__author__ = 'Tan'


# This is the evaluation method for text recognition.
def levenshtein_distance(ground_truth, result, add=0, delete=0, sub=0):
    len_gt = len(ground_truth)
    len_result = len(result)

    # Each item means (cost, add, delete, sub)
    cost_matrix = [[(0, 0, 0, 0) for x in range(len_gt + 1)] for x in range(len_result + 1)]

    for i in range(0, len_gt + 1):
        cost_matrix[0][i] = (i, i, 0, 0)

    for i in range(0, len_result + 1):
        cost_matrix[i][0] = (i, 0, i, 0)

    for i in range(0, len_gt):
        for j in range(0, len_result):
            cost = 0 if ground_truth[i] == result[j] else 1

            cost_list = []
            item = cost_matrix[j + 1][i]
            cost_list.append((item[0] + 1, item[1] + 1, item[2], item[3]))

            item = cost_matrix[j][i + 1]
            cost_list.append((item[0] + 1, item[1], item[2] + 1, item[3]))

            item = cost_matrix[j][i]
            cost_list.append((item[0] + cost, item[1], item[2], item[3] + cost))

            min_dist = min(cost_list, key=lambda c: float(c[0]) + float(c[2] + c[3]) / 1000)
            cost_matrix[j + 1][i + 1] = (min_dist)

    # if len_gt == 0:
    #     return [len_result, add, delete + len_result, sub]
    # if len_result == 0:
    #     return [len_gt, add + len_gt, delete, sub]
    #
    # cost = 1
    # if ground_truth[len_gt - 1] == result[len_result - 1]:
    #     cost = 0
    #
    # dist_list = [levenshtein_distance(ground_truth[0:-1], result, add + 1, delete, sub),
    #              levenshtein_distance(ground_truth, result[0:-1], add, delete + 1, sub),
    #              levenshtein_distance(ground_truth[0:-1], result[0:-1], add, delete, sub + cost)]
    #
    # dist_list[0][0] += 1
    # dist_list[1][0] += 1
    # dist_list[2][0] += cost
    #
    # min_dist = min(dist_list, key=lambda c: c[0])
    return cost_matrix[len_result][len_gt]


def get_characters_edit_distance(gt_list):
    total_edit_distance = 0

    for gt in gt_list:
        for distance in gt['recognition']:
            total_edit_distance += distance

    return float(total_edit_distance)


def get_word_precision(result_rect_list):
    total_characters = 0
    delete_and_editing = 0

    for rect in result_rect_list:
        total_characters += len(rect.text)
        delete_and_editing += rect.editing[1] + rect.editing[2]

    print 'levenshtein distance precision: %f' % (float(total_characters - delete_and_editing) / total_characters)


def get_word_recall(gt_list):
    total_characters = 0
    delete_and_editing = 0

    for gt in gt_list:
        rects = gt['rects']
        for j in range(0, len(rects)):
            text = rects[j].text
            total_characters += len(text)
            delete_and_editing += gt['editing'][j][1] + gt['editing'][j][2]

    print 'levenshtein distance recall: %f' % (float(total_characters - delete_and_editing) / total_characters)


if __name__ == '__main__':
    s1 = 'tantianxiang'
    s2 = 'tamtincheung'

    ret = levenshtein_distance(s1, s2)
    print ret
