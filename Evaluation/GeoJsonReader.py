from Geometry.ResultRectangle import *
from Geometry.GroundTruthRectangle import *


__author__ = 'Tan'


def get_result_rectangles(geojson_obj):
    rectangle_list = []
    for feature in geojson_obj['features']:
        if 'TextNonText' in feature:
            text = feature['TextNonText']
        else:
            if 'properties' in feature and 'text' in feature['properties']:
                text = feature['properties']['text']
            elif 'NameAfterDictionary' in feature:
                text = feature['NameAfterDictionary']
            else:
                print 'Invalid file format.'
                quit()

        if text == 'NonText':
            text = ""
        for coordinate in feature['geometry']['coordinates']:
            rect = ResultRectangle(coordinate, text)
            rectangle_list.append(rect)

    return rectangle_list


def get_ground_truth_list(gt_obj):
    gt_list = []
    for feature in gt_obj['features']:
        feature['properties'] = dict((k.lower(), v) for k, v in feature['properties'].iteritems())
        if 'label' in feature['properties']:
            feature['properties']['text'] = feature['properties'].pop('label')

    for feature in gt_obj['features']:
        if feature['properties']['id'] == 1:
            new_phrase_dict = {}
            phrase = feature['properties']['phrase']
            total_words = len(phrase.split(' '))
            new_gt_rect = GroundTruthRectangle(feature['properties']['id'], feature['properties']['text'],
                                               feature['properties']['phrase'], feature['geometry']['coordinates'][0])

            new_phrase_dict['phrase'] = phrase
            new_phrase_dict['rects'] = [new_gt_rect]
            new_phrase_dict['found'] = [False]
            new_phrase_dict['recognition'] = [0]        # Recognition stroes the Levenshtein distance.
            new_phrase_dict['editing'] = [(0, 0, 0)]    # Editing stores the operations needed to perform in Levenshtein distance.

            for i in range(2, total_words + 1, 1):
                for f2 in gt_obj['features']:
                    if f2['properties']['phrase'] == phrase and f2['properties']['id'] == i:
                        new_gt_rect = GroundTruthRectangle(f2['properties']['id'], f2['properties']['text'],
                                                           f2['properties']['phrase'], f2['geometry']['coordinates'][0])
                        new_phrase_dict['rects'].append(new_gt_rect)
                        new_phrase_dict['found'].append(False)
                        new_phrase_dict['recognition'].append(0)
                        new_phrase_dict['editing'].append([0, 0, 0])

            gt_list.append(new_phrase_dict)

    return gt_list
