import os
from Evaluation.GeoJsonReader import *
from Evaluation.ResultEvaluation import *
import sys


__author__ = 'Tan'
sys.stdout = open("./Statistics/log.txt", "w")


def load_json_file(filename):
    plain_text = open(filename).read()

    # Make the plain text can be recognized by the json module.
    plain_text = plain_text.replace(',}', '}')
    plain_text = plain_text.replace(',]', ']')
    plain_text = plain_text.replace('}]}]}', '}]}')
    plain_text = " ".join(plain_text.split("\n"))

    data = json.loads(plain_text)
    return data


def evaluate_result(gt_obj, result_obj):
    gt_list = get_ground_truth_list(gt_obj)
    result_rect_list = get_result_rectangles(result_obj)

    return text_eval(result_rect_list, gt_list)


if __name__ == '__main__':
    for i in range(1, 8):
        groundTruthFile = './GroundTruths/1920_' + i.__str__() + '.geojson'
        resultFile = './Results/1920-' + i.__str__() + '.pngByPixels.txt'
        print "Ground Truth: %s\nResult: %s" % (groundTruthFile, resultFile)
        result_obj = load_json_file(resultFile)
        gt_obj = load_json_file(groundTruthFile)
        overlap_json, non_overlap_json = evaluate_result(gt_obj, result_obj)
        output_json_file(overlap_json, 'eval_overlap' + resultFile.split('/')[-1])
        output_json_file(non_overlap_json, 'eval_nonoverlap' + resultFile.split('/')[-1], False)
