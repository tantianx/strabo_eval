from GeoRectangle import *


__author__ = 'Tan'


class GroundTruthRectangle(GeoRectangle):
    def __init__(self, location, text, phrase, coordinates):
        self.id = location
        self.phrase = phrase

        GeoRectangle.__init__(self, coordinates, text)
